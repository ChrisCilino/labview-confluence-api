﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">PetranWay_Confluence API.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../PetranWay_Confluence API.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Add Command Prefix.vi" Type="VI" URL="../Add Command Prefix.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'V!!!!#Q";1(!!(A!!/RZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7);;5.P&lt;G:M&gt;76O9W5A27ZU;82Z,GRW9WRB=X-!&amp;7F$&lt;WZG&lt;(6F&lt;G.F)%^C;G6D&gt;#"J&lt;A!%!!!!8%"Q!"Y!!$M?5'6U=G&amp;O6W&amp;Z8U.P&lt;G:M&gt;76O9W5A16"*,GRW&lt;'FC'GF$&lt;WZG&lt;(6F&lt;G.F)%6O&gt;'FU?3ZM&gt;G.M98.T!":J1W^O:GRV:7ZD:3"09GJF9X1A&lt;X6U!!!11$$`````"U.P&lt;7VB&lt;G1!'E!Q`````R"1=G6G;8AA+S"$&lt;WVN97ZE!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!&amp;!!9!"R.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!5!"A!(#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!Q!%!!%!!1!"!!%!#!!"!!%!!1!*!A!"#!!!E!!!!!!!!!!!!!!!!!!!!)U!!!))!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1084236288</Property>
	</Item>
	<Item Name="Read Entity ID.vi" Type="VI" URL="../Read Entity ID.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!-0````]#351!!(Z!=!!?!!!\(F"F&gt;(*B&lt;F&gt;B?6^$&lt;WZG&lt;(6F&lt;G.F)%&amp;133ZM&gt;GRJ9BJJ1W^O:GRV:7ZD:3"&amp;&lt;H2J&gt;(EO&lt;(:D&lt;'&amp;T=Q!Y5'6U=G&amp;O6W&amp;Z8U&amp;U&lt;'&amp;T=WFB&lt;C""5%EO&lt;(:M;7)[;5.P&lt;G:M&gt;76O9W5A4W*K:7.U,GRW9WRB=X-!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!9#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1084236288</Property>
	</Item>
	<Item Name="Read iConfluence.vi" Type="VI" URL="../Read iConfluence.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!("!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%Z!=!!?!!!U(F"F&gt;(*B&lt;F&gt;B?6^$&lt;WZG&lt;(6F&lt;G.F)%&amp;133ZM&gt;GRJ9B.J1W^O:GRV:7ZD:3ZM&gt;G.M98.T!!!0;5.P&lt;G:M&gt;76O9W5A4X6U!&amp;R!=!!?!!!\(F"F&gt;(*B&lt;F&gt;B?6^$&lt;WZG&lt;(6F&lt;G.F)%&amp;133ZM&gt;GRJ9BJJ1W^O:GRV:7ZD:3"&amp;&lt;H2J&gt;(EO&lt;(:D&lt;'&amp;T=Q!7;5.P&lt;G:M&gt;76O9W5A4W*K:7.U)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/RZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7);;5.P&lt;G:M&gt;76O9W5A27ZU;82Z,GRW9WRB=X-!&amp;7F$&lt;WZG&lt;(6F&lt;G.F)%^C;G6D&gt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1084236288</Property>
	</Item>
</LVClass>
