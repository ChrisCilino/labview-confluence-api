﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">PetranWay_Confluence API.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../PetranWay_Confluence API.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Parent Libraries" Type="Parent Libraries">
		<Item Name="PetranWay_Confluence API.lvlib:iConfluence Entity.lvclass" Type="Parent" URL="../../iConfluence Object/iConfluence Entity.lvclass"/>
	</Item>
	<Item Name="Update JSON Property Description.vi" Type="VI" URL="../Update JSON Property Description.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'B!!!!#Q"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!!1!!!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!VJ5(*P='6S&gt;(EA&lt;X6U!"J!-0````]25(*P='6S&gt;(EA3W6Z)%ZB&lt;75!(E!Q`````R21=G^Q:8*U?3"%:8.D=GFQ&gt;'FP&lt;A!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"1!'!!=4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!&amp;!!9!"QFF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!!1!%!!%!!1!"!!A!!1!"!!%!#1-!!1A!!*)!!!!!!!!!!!!!!!!!!!#.!!!!%!!!!!!!!!)!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!U,!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Create JSON Property.vi" Type="VI" URL="../Create JSON Property.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!#Q"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!"2!-0````],2'6T9X*J=(2J&lt;WY!"!!!!%J!=!!?!!!S(F"F&gt;(*B&lt;F&gt;B?6^$&lt;WZG&lt;(6F&lt;G.F)%&amp;133ZM&gt;GRJ9B&amp;J5(*P='6S&gt;(EO&lt;(:D&lt;'&amp;T=Q!!$7F1=G^Q:8*U?3"P&gt;81!&amp;E!Q`````QV1=G^Q:8*U?3"/97VF!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!5!"A!(%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"1!'!!=*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!)!!Q!%!!)!!A!#!!)!!A!)!!)!!A!#!!E$!!%)!!#3!!!##!!!!!!!!!!!!!!!D1!!!B!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Update JSON Property Value.vi" Type="VI" URL="../Update JSON Property Value.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!"B!-0````]/&gt;8"E982F)'.P&lt;7VF&lt;H1!!!1!!!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!VJ5(*P='6S&gt;(EA&lt;X6U!"J!-0````]25(*P='6S&gt;(EA3W6Z)%ZB&lt;75!+%!Q`````R^797RV:3!]3F.04H2F?(1_)#BE:7:B&gt;7RU07ZV&lt;'QJ!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!9!"Q!)%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"A!(!!A*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!)!!Q!%!!)!"1!#!!)!!A!*!!)!!A!#!!I$!!%)!!#3!!!##!!!!!!!!!!!!!!!D1!!!"!!!!!!!!!"#A!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Create Simple Property.vi" Type="VI" URL="../Create Simple Property.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!!1!!!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!VJ5(*P='6S&gt;(EA&lt;X6U!":!-0````].5(*P='6S&gt;(EA4G&amp;N:1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!%!!5!"B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!Q!"!!%!!1!"!!%!"Q!"!!%!!1!)!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!)1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Update Simple Property Value.vi" Type="VI" URL="../Update Simple Property Value.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'^!!!!$!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!"B!-0````]/&gt;8"E982F)'.P&lt;7VF&lt;H1!!!1!!!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!VJ5(*P='6S&gt;(EA&lt;X6U!"J!-0````]25(*P='6S&gt;(EA3W6Z)%ZB&lt;75!)E"4(6:B&lt;(6F)#B4&gt;(*J&lt;G=M)%*P&lt;WQM)%FO&gt;'6H:8)J!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!9!"Q!)%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"A!(!!A*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!#!!)!!Q!%!!)!"1!#!!)!!A!*!!)!!A!#!!I$!!%)!!#3!!!##!!!!!!!!!!!!!!!D1!!!"!!!!!!!!!!%A!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!#Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Get Property Value.vi" Type="VI" URL="../Get Property Value.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-U!!!!&amp;Q"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!!1!!!!C1$$`````'5J44UYA5(*P='6S&gt;(EA2'6T9X*J=(2J&lt;WY!3E"Q!"Y!!$)?5'6U=G&amp;O6W&amp;Z8U.P&lt;G:M&gt;76O9W5A16"*,GRW&lt;'FC%7F1=G^Q:8*U?3ZM&gt;G.M98.T!!!.;6"S&lt;X"F=H2Z)'^V&gt;!!71$$`````$6"S&lt;X"F=H2Z)%ZB&lt;75!'%!Q`````QZ1=G^Q:8*U?3"797RV:1!!$%!Q`````Q*J:!!!$%!Q`````Q.L:8E!$E!Q`````Q2X;'6O!!!71$$`````$4R66%9Y0GVF=X.B:W5!$5!$!!:O&gt;7VC:8)!!!Z!)1FN;7ZP=E6E;81!'%!B%W.P&lt;H2F&lt;H25?8"F47^E;7:J:71!=1$R!!!!!!!!!!-?5'6U=G&amp;O6W&amp;Z8U.P&lt;G:M&gt;76O9W5A16"*,GRW&lt;'FC$&amp;"B:W5O&lt;(:D&lt;'&amp;T=S"1=G^Q:8*U?3"7:8*T;7^O)%FO:G^S&lt;7&amp;U;7^O,G.U&lt;!!=1&amp;!!"1!)!!E!#A!,!!Q(&gt;G6S=WFP&lt;A"N!0%!!!!!!!!!!RZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)-5'&amp;H:3ZM&gt;G.M98.T&amp;%.P&lt;H2F&lt;H1A5(*P='6S&gt;(EO9X2M!#2!5!!$!!9!"Q!.%F"S&lt;X"F=H2Z)%VF&gt;'&amp;E982B)!!!&amp;%!B$V"S&lt;X"F=H2Z)%:P&gt;7ZE0Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!1!"%!%B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!)22797RV:3"J=S"+5U^/)%^C;G6D&gt;!!!&amp;E"1!!-!%!!2!")*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!)!!Q!%!!5!!1!/!!%!$Q!4!!%!!1!5!"5$!!%)!!#3!!!!!!!!!!!!!!!*!!!!D1!!!"!!!!!*!!!!!!!!!!E!!!!!!!!!#1!!!!I!!!!!!!!!!!!!!!E!!!!.#Q!!!!%!&amp;A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Get Property Names.vi" Type="VI" URL="../Get Property Names.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!!1!!!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!VJ5(*P='6S&gt;(EA&lt;X6U!#&gt;!"!!A6'^U97QA5G6T&gt;7RU=S"3:8&amp;V:8.U:71A+%&amp;M&lt;$UN-3E!!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!"!Z1=G^Q:8*U?3"/97VF=Q!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"A!(!!A4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71&amp;!!!Q!'!!=!#!FF=H*P=C"P&gt;81!&lt;!$Q!"!!!!!"!!%!!1!#!!-!"1!"!!%!!1!"!!E!!1!"!!%!#A-!!1A!!*)!!!!!!!!!!!!!!!!!!!#.!!!!#!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!U,!!!!!1!,!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Delete Property.vi" Type="VI" URL="../Delete Property.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!#A"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!RJ5(*P='6S&gt;(EA;7Y!!!1!!!"+1(!!(A!!-BZ1:82S97Z898F@1W^O:GRV:7ZD:3""5%EO&lt;(:M;7)2;6"S&lt;X"F=H2Z,GRW9WRB=X-!!!VJ5(*P='6S&gt;(EA&lt;X6U!":!-0````].5(*P='6S&gt;(EA4G&amp;N:1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!%!!5!"B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!1!"1!'#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!Q!"!!%!!1!"!!%!"Q!"!!%!!1!)!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.MustOverride" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
</LVClass>
