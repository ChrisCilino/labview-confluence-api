﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.FriendGUID" Type="Str">136f6167-dc93-41f8-8833-5c5bb2b22ed0</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="PetranWayTag:-:PetranWayAutoDocPrinterReportComboTag:-:PetranWay_Documentation Reports.lvlib:Library Report.lvclass::PetranWay_Documentation Reports.lvlib:BitBucket Wiki Markdown.lvclass" Type="Str">C:\PetranWay\Wikis\LabVIEW Atlassian API\Developer Resources\APIs\PetranWay_Atlassian API Library Documentation.md</Property>
	<Item Name="HTML" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Add Link to Text" Type="Folder">
			<Item Name="Add URL to Text.vi" Type="VI" URL="../Confluence/HTML/Add Link to Text/Add URL to Text.vi"/>
			<Item Name="Add Anchor To Text.vi" Type="VI" URL="../Confluence/HTML/Add Link to Text/Add Anchor To Text.vi"/>
		</Item>
		<Item Name="Create Table" Type="Folder">
			<Item Name="Create Table (Fixed).vi" Type="VI" URL="../Confluence/HTML/Create Table (Fixed).vi"/>
			<Item Name="Create Table (Responsive).vi" Type="VI" URL="../Confluence/HTML/Create Table (Responsive).vi"/>
		</Item>
		<Item Name="Parse Table From Page" Type="Folder">
			<Item Name="Parse Cells in Row.vi" Type="VI" URL="../Confluence/HTML/Parse Table From Page/Parse Cells in Row.vi"/>
			<Item Name="Parse Rows In Table.vi" Type="VI" URL="../Confluence/HTML/Parse Table From Page/Parse Rows In Table.vi"/>
			<Item Name="Table Parser Template.vi" Type="VI" URL="../Confluence/HTML/Parse Table From Page/Table Parser Template.vi"/>
			<Item Name="Parse Table From Page.vi" Type="VI" URL="../Confluence/HTML/Parse Table From Page/Parse Table From Page.vi"/>
			<Item Name="Parse Table Header.vi" Type="VI" URL="../Confluence/HTML/Parse Table From Page/Parse Table Header.vi"/>
		</Item>
		<Item Name="HTML Tags" Type="Folder">
			<Item Name="Find HTML Tags.vi" Type="VI" URL="../Confluence/HTML/HTML Tags/Find HTML Tags.vi"/>
			<Item Name="Remove HTML Tags.vi" Type="VI" URL="../Confluence/HTML/HTML Tags/Remove HTML Tags.vi"/>
			<Item Name="HTML Tag Catpure Regular Expression.vi" Type="VI" URL="../Confluence/HTML/HTML Tags/HTML Tag Catpure Regular Expression.vi"/>
		</Item>
		<Item Name="Add Link to Text.vi" Type="VI" URL="../Confluence/HTML/Add Link to Text.vi"/>
		<Item Name="Create Table.vi" Type="VI" URL="../Confluence/HTML/Create Table.vi"/>
		<Item Name="Embed In Multiexcerpt Include.vi" Type="VI" URL="../Confluence/HTML/Embed In Multiexcerpt Include.vi"/>
		<Item Name="Get Multiexcerpt HTML from Page and Name.vi" Type="VI" URL="../Confluence/HTML/Get Multiexcerpt HTML from Page and Name.vi"/>
		<Item Name="Generate Image HTML.vi" Type="VI" URL="../Confluence/HTML/Generate Image HTML.vi"/>
		<Item Name="Overwrite Table Under Header.vi" Type="VI" URL="../Confluence/HTML/Overwrite Table Under Header.vi"/>
		<Item Name="Replace Text Under Header.vi" Type="VI" URL="../Confluence/HTML/Replace Text Under Header.vi"/>
		<Item Name="Replace With Escape Chars.vi" Type="VI" URL="../Confluence/HTML/Replace With Escape Chars.vi"/>
		<Item Name="Replace All With Escape Chars.vi" Type="VI" URL="../Confluence/HTML/Replace All With Escape Chars.vi"/>
		<Item Name="Create Anchor.vi" Type="VI" URL="../Confluence/HTML/Create Anchor.vi"/>
		<Item Name="Find Macro In Page.vi" Type="VI" URL="../Confluence/HTML/Find Macro In Page.vi"/>
		<Item Name="Find Macro In Page Template.vi" Type="VI" URL="../Confluence/HTML/Find Macro In Page Template.vi"/>
		<Item Name="Carriage Return Constant.vi" Type="VI" URL="../Confluence/HTML/Carriage Return Constant.vi"/>
		<Item Name="Replace Escape Chars.vi" Type="VI" URL="../Confluence/HTML/Replace Escape Chars.vi"/>
		<Item Name="Generate Warning HTML.vi" Type="VI" URL="../Confluence/HTML/Generate Warning HTML.vi"/>
	</Item>
	<Item Name="Template" Type="Folder">
		<Item Name="Template Query Result.ctl" Type="VI" URL="../Confluence/Space/TypeDefs/Template Query Result.ctl"/>
		<Item Name="Template.ctl" Type="VI" URL="../Confluence/Space/TypeDefs/Template.ctl"/>
	</Item>
	<Item Name="Participants" Type="Folder">
		<Item Name="User" Type="Folder">
			<Item Name="User.lvclass" Type="LVClass" URL="../Participants/User/User.lvclass"/>
			<Item Name="User Data Center.lvclass" Type="LVClass" URL="../Data Center/User/User Data Center.lvclass"/>
			<Item Name="User Cloud.lvclass" Type="LVClass" URL="../Cloud/User/User Cloud.lvclass"/>
		</Item>
		<Item Name="Group" Type="Folder">
			<Item Name="Group.lvclass" Type="LVClass" URL="../Participants/Group/Group.lvclass"/>
			<Item Name="Group Cloud.lvclass" Type="LVClass" URL="../Cloud/Group/Group Cloud.lvclass"/>
			<Item Name="Group Data Center.lvclass" Type="LVClass" URL="../Data Center/Group/Group Data Center.lvclass"/>
		</Item>
		<Item Name="Participant.lvclass" Type="LVClass" URL="../Participants/Participant/Participant.lvclass"/>
	</Item>
	<Item Name="Confluence" Type="Folder">
		<Item Name="iConfluence Entity" Type="Folder">
			<Item Name="iConfluence Entity.lvclass" Type="LVClass" URL="../iConfluence Object/iConfluence Entity.lvclass"/>
		</Item>
		<Item Name="Confluence" Type="Folder">
			<Item Name="iConfluence.lvclass" Type="LVClass" URL="../Confluence/iConfluence/iConfluence.lvclass"/>
			<Item Name="Confluence.lvclass" Type="LVClass" URL="../Confluence/Confluence Connection/Confluence.lvclass"/>
			<Item Name="Confluence Cloud.lvclass" Type="LVClass" URL="../Cloud/Confluence Cloud/Confluence Cloud.lvclass"/>
			<Item Name="Confluence Data Center.lvclass" Type="LVClass" URL="../Data Center/Confluence Data Center/Confluence Data Center.lvclass"/>
		</Item>
		<Item Name="Page" Type="Folder">
			<Item Name="Page.lvclass" Type="LVClass" URL="../Confluence/Page/Page.lvclass"/>
			<Item Name="Page Cloud.lvclass" Type="LVClass" URL="../Cloud/Page/Page Cloud.lvclass"/>
			<Item Name="Page Data Center.lvclass" Type="LVClass" URL="../Data Center/Page/Page Data Center.lvclass"/>
		</Item>
		<Item Name="Space" Type="Folder">
			<Item Name="Space.lvclass" Type="LVClass" URL="../Confluence/Space/Space.lvclass"/>
			<Item Name="Space Cloud.lvclass" Type="LVClass" URL="../Cloud/Space/Space Cloud.lvclass"/>
			<Item Name="Space Data Center.lvclass" Type="LVClass" URL="../Data Center/Space/Space Data Center.lvclass"/>
		</Item>
		<Item Name="iProperty" Type="Folder">
			<Item Name="iProperty.lvclass" Type="LVClass" URL="../iProperty/iProperty.lvclass"/>
		</Item>
	</Item>
</Library>
